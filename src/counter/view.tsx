import { h, View } from "hyperapp";
// import { h as _h, View } from "hyperapp";
import { State } from "./state";
import { Actions } from "./actions";

// const h = _h;

const view: View<State, Actions> = (state, actions) => (
  <div oncreate={() => console.log("Hurrá!")}>
    <h1>{state.counter.count}</h1>
    <button onclick={() => actions.counter.down(1)}>-</button>
    <button onclick={() => actions.counter.up(1)}>+</button>
  </div>
);

export default view;
