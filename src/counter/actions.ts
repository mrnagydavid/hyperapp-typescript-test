import { ActionResult, ActionsType } from "hyperapp";
import { State } from "./state";

export interface Actions {
  counter: {
    down: (value: number) => (state: State) => ActionResult<State>;
    up: (value: number) => (state: State) => ActionResult<State>;
  };
}

const actions: ActionsType<State, Actions> = {
  counter: {
    down: (value) => (state) => ({ count: state.count - value }),
    up: (value) => (state) => ({ count: state.count + value }),
  },
};

export default actions;
