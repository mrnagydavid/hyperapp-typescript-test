export interface State {
  counter: {
    count: number;
  };
}

const state: State = {
  counter: {
    count: 0,
  },
};

export default state;
