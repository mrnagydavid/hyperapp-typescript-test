import { h, app } from "hyperapp";
import counterState, { State } from "./state";
import counterActions, { Actions } from "./actions";
import counterView from "./view";

export default (container: Element) => {
  console.log("Hello counter!");
  return app<State, Actions>(counterState, counterActions, counterView, container);
}

